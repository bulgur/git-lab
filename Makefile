.phony: tests ut clean coverage

.covs:
	mkdir .covs

.covs/ut: .covs
	python -m coverage run --branch -m unittest discover -s tests/ut
	mv .coverage .covs/ut

ut: .covs/ut

clean:
	rm -rf .covs
	find -iname "*.pyc" | xargs -r -L1 rm -rf
	find -iname "__pycache__" | xargs -r -L1 rmdir

tests: clean ut

coverage: tests
	python -m coverage combine .covs/*
	python -m coverage report $(shell find glp -iname "*.py")
