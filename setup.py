from setuptools import setup

VERSION = '0.1.0'

CLASSIFIERS = """
Development Status :: 3 - Alpha
Intended Audience :: Developers
License :: OSI Approved :: MIT License
Programming Language :: Python :: 3.6
Programming Language :: Python :: 3.7
Topic :: Utilities
""".strip()

DESCRIPTION = """
Git plugin to manage upstream project hosted on Gitlab
from command line
"""

URL = 'https://gitlab.com/bulgur/git-lab'

DOWNLOAD_URL = f'{URL}/-/archive/{VERSION}/git-lab-{VERSION}.tar.gz'

setup(
    author="Paweł Tomak",
    author_email="pawel@tomak.eu",
    classifiers=CLASSIFIERS,
    description=DESCRIPTION,
    install_requires=[
        'GitPython',
        'python-gitlab'
    ],
    name='glp',
    packages=['glp'],
    scripts=[
        'bin/git-lab'
    ],
    version=VERSION
)
