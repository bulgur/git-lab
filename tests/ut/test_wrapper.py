from unittest import TestCase
from unittest.mock import MagicMock, patch

from gitlab.exceptions import GitlabAuthenticationError, GitlabGetError

from glp.wrapper import GitlabWrapper
from glp import GitLabPluginError


class TestGitlabWrapper(TestCase):

    def setUp(self):
        self.patchers = {
            'gitlab_config': patch('glp.wrapper.GitLabConfig'),
            'gitlab_gitlab': patch('gitlab.Gitlab'),
            'output': patch('glp.wrapper.get_output')
        }
        self.mocks = {k: v.start() for k, v in self.patchers.items()}
        self.project = MagicMock()
        self.output = self.mocks["output"]
        self.output.return_value = "the token"
        self.gitlab = self.mocks["gitlab_gitlab"].return_value
        self.gitlab.projects.get.return_value = self.project
        self.config = self.mocks["gitlab_config"].return_value
        self.config.project_path = 'project/path'
        self.config.gitlab_url = "gitlab url"
        self.config.personal_token_cmd = "cmd token"

    def tearDown(self):
        for v in self.mocks.values():
            v.stop()

    def test_init(self):
        wrapper = GitlabWrapper()
        assert wrapper._gitlab is None
        assert isinstance(wrapper.config, MagicMock)

    def test__gitlab_from_token(self):
        wrapper = GitlabWrapper()
        wrapper._gitlab_from_token()
        self.output.assert_called_once_with("cmd token")
        assert wrapper._gitlab is not None
        self.mocks["gitlab_gitlab"].assert_called_once_with(
            url="gitlab url",
            private_token="the token"
        )

    def test__gitlab_from_token_output_exception(self):
        self.output.side_effect = GitLabPluginError("runtime error")
        wrapper = GitlabWrapper()
        wrapper._gitlab_from_token()
        self.output.assert_called_once_with("cmd token")
        assert wrapper._gitlab is None
        self.mocks["gitlab_gitlab"].assert_not_called()

    @patch('glp.wrapper.GitlabWrapper._gitlab_annonymously')
    @patch('glp.wrapper.GitlabWrapper._gitlab_from_token')
    def test_auth_personal_token(self, auth_, annon_):
        wrapper = GitlabWrapper()
        wrapper._gitlab = self.gitlab
        wrapper.auth()
        auth_.assert_called_once_with()
        annon_.assert_not_called()
        wrapper._gitlab.auth.assert_called_once_with()

    @patch('glp.wrapper.GitlabWrapper._gitlab_annonymously')
    @patch('glp.wrapper.GitlabWrapper._gitlab_from_token')
    def test_auth_without_personal_token(self, auth_, annon_):
        wrapper = GitlabWrapper()
        wrapper.config.personal_token_cmd = None
        wrapper._gitlab = self.gitlab
        wrapper.auth()
        auth_.assert_not_called()
        annon_.assert_called_once_with()
        wrapper._gitlab.auth.assert_called_once_with()

    @patch('glp.wrapper.GitlabWrapper._gitlab_annonymously')
    @patch('glp.wrapper.GitlabWrapper._gitlab_from_token')
    def test_auth_no_gitlab_instance_created(self, auth_, annon_):
        wrapper = GitlabWrapper()
        wrapper.config.personal_token_cmd = None
        with self.assertRaises(GitLabPluginError) as exception:
            wrapper.auth()

    @patch('glp.wrapper.GitlabWrapper._gitlab_annonymously')
    @patch('glp.wrapper.GitlabWrapper._gitlab_from_token')
    def test_auth_authentication_error(self, auth_, annon_):
        wrapper = GitlabWrapper()
        wrapper.config.personal_token_cmd = None
        wrapper._gitlab = self.gitlab
        wrapper._gitlab.auth.side_effect = GitlabAuthenticationError(
            error_message="error message",
            response_code=404,
            response_body="{status: 404}"
        )
        with self.assertRaises(GitLabPluginError) as exception:
            wrapper.auth()
        assert "Status: 404" in str(exception.exception)
        assert "Message: error message" in str(exception.exception)
        assert "Response: {status: 404}" in str(exception.exception)

    @patch('glp.wrapper.GitlabWrapper._gitlab_annonymously')
    @patch('glp.wrapper.GitlabWrapper._gitlab_from_token')
    def test_auth_get_error(self, auth_, annon_):
        wrapper = GitlabWrapper()
        wrapper.config.personal_token_cmd = None
        wrapper._gitlab = self.gitlab
        wrapper._gitlab.auth.side_effect = GitlabGetError(
            "error message", 403, "{status: 403}"
        )
        with self.assertRaises(GitLabPluginError) as exception:
            wrapper.auth()
        assert "Status: 403" in str(exception.exception)
        assert "Message: error message" in str(exception.exception)
        assert "Response: {status: 403}" in str(exception.exception)

    def test_project(self):
        wrapper = GitlabWrapper()
        project = wrapper.project()
        assert project == self.project
        self.gitlab.projects.get.assert_called_once_with('project/path')
        self.gitlab.auth.assert_called_once_with()

    def test_project_no_auth(self):
        wrapper = GitlabWrapper()
        wrapper._gitlab = self.gitlab
        project = wrapper.project()
        assert project == self.project
        self.gitlab.projects.get.assert_called_once_with('project/path')
        self.gitlab.auth.assert_not_called()

    def test_project_raise(self):
        self.gitlab.projects.get.side_effect = GitlabGetError(
            "error message", 403, "{status: 403}"
        )
        wrapper = GitlabWrapper()
        with self.assertRaises(GitLabPluginError) as exception:
            wrapper.project()
        assert "Status: 403" in str(exception.exception)
        assert "Message: error message" in str(exception.exception)
        assert "Response: {status: 403}" in str(exception.exception)
