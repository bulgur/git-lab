import argparse
import json
from unittest import TestCase
from unittest.mock import MagicMock, patch, call

import yaml
from gitlab.exceptions import GitlabGetError

from glp import GitLabPluginError, Opts
from glp.commands import CommandBase, Config, Issue, Lint, Mr


def return_list(*args):
    def inner(*innerargs, **kwargs):
        return args
    return inner


def get_test_parsers():
    parser = argparse.ArgumentParser(
        prog="git-lab",
        description="lorem ipsum"
    )
    commands = parser.add_subparsers(
        dest='command',
        help="Subcommands"
    )
    return parser, commands


class Args(Opts):

    def __init__(self, dct=dict()):
        for key, value in dct.items():
            setattr(self, key, value)


class TestCommandBase(TestCase):

    def test_command_name(self):
        assert CommandBase.command_name() == "commandbase"

    def test_get_parser(self):
        with self.assertRaises(NotImplementedError) as exception:
            CommandBase.get_parser(MagicMock())

    def test_run(self):
        with self.assertRaises(NotImplementedError) as exception:
            CommandBase(MagicMock()).run()


class TestConfigCommand(TestCase):

    def test_get_parser(self):
        parser, commands = get_test_parsers()
        config_parser = Config.get_parser(commands)
        url = 'https://gitlab.test.org'
        assert vars(config_parser.parse_args(['-u', 'user', url])) == {
            'personal_token_cmd': None,
            'project_path': None,
            'local': True,
            'gitlab': url,
            'user': 'user'
        }
        args = [
            'config',
            '-g',
            '-u',
            'user',
            '--personal-token-cmd',
            'token',
            '--project-path',
            'group/name',
            url
        ]
        assert vars(parser.parse_args(args)) == {
            'command': 'config',
            'personal_token_cmd': 'token',
            'project_path': 'group/name',
            'local': False,
            'gitlab': url,
            'user': 'user'
        }

    def test_get_parser_raise_system_exit(self):
        parser, commands = get_test_parsers()
        config_parser = Config.get_parser(commands)
        url = 'https://gitlab.test.org'
        with patch('sys.stderr') as stderr:
            with self.assertRaises(SystemExit) as exception:
                config_parser.parse_args([url])
            with self.assertRaises(SystemExit) as exception:
                config_parser.parse_args(['-l', 'user'])

    @patch('glp.commands.GitLabConfig')
    def test_run(self, git_lab_config):
        args = MagicMock()
        args.personal_token_cmd = "A command"
        args.gitlab = 'a gitlab url'
        args.local = False
        args.user = 'gitlab user'
        args.project_path = 'group/project'
        config = Config(args)
        config.run()
        assert git_lab_config.return_value.personal_token_cmd == "A command"
        assert git_lab_config.return_value.gitlab_url == 'a gitlab url'
        assert git_lab_config.return_value.user == 'gitlab user'
        assert git_lab_config.return_value.project_path == 'group/project'

    @patch('glp.commands.GitLabConfig')
    def test_run_without_token_arg(self, git_lab_config):
        git_lab_config.return_value.personal_token_cmd = 'aqq'
        args = MagicMock()
        args.local = False
        args.personal_token_cmd = None
        args.gitlab = 'a gitlab url'
        config = Config(args)
        config.run()
        assert git_lab_config.return_value.personal_token_cmd == 'aqq'
        assert git_lab_config.return_value.gitlab_url == 'a gitlab url'

    @patch('glp.commands.GitLabConfig')
    def test_run_without_gitlab_url(self, git_lab_config):
        args = MagicMock()
        args.local = False
        args.gitlab = None
        config = Config(args)
        with self.assertRaises(GitLabPluginError) as exception:
            config.run()
        assert exception.exception.status is 1

    @patch('glp.commands.GitLabConfig')
    def test_run_without_project_path_arg(self, git_lab_config):
        git_lab_config.return_value.project_path = 'group/aqq'
        args = MagicMock()
        args.local = False
        args.project_path = None
        args.gitlab = 'a gitlab url'
        config = Config(args)
        config.run()
        assert git_lab_config.return_value.project_path == 'group/aqq'
        assert git_lab_config.return_value.gitlab_url == 'a gitlab url'


class TestIssueCommandParser(TestCase):

    def test_parser(self):
        parser, commands = get_test_parsers()
        issue_parser = Issue.get_parser(commands)
        args = []
        expected = {
            'subcommand': None,
        }
        assert vars(issue_parser.parse_args(args)) == expected
        args.insert(0, 'issue')
        expected["command"] = 'issue'
        assert vars(parser.parse_args(args)) == expected


class TestIssueCommandCreateParser(TestCase):

    def setUp(self):
        self.expected = {
            'subcommand': 'create',
            'labels': None,
            'milestone': None,
            'assignees': None,
        }

    def test_parser_create(self):
        parser, commands = get_test_parsers()
        create_parser = Issue.get_parser(commands)
        args = ['create']
        assert vars(create_parser.parse_args(args)) == self.expected
        args.insert(0, 'issue')
        self.expected["command"] = 'issue'
        assert vars(parser.parse_args(args)) == self.expected


class TestIssueCommandEditParser(TestCase):

    def setUp(self):
        self.expected = {
            'subcommand': 'edit',
            'addlabels': None,
            'setlabels': None,
            'editor': False,
            'id': '1',
            'milestone': None,
            'assignees': None,
        }

    def test_parser_edit(self):
        parser, commands = get_test_parsers()
        edit_parser = Issue.get_parser(commands)
        args = ['edit', '1']
        assert vars(edit_parser.parse_args(args)) == self.expected
        args.insert(0, 'issue')
        self.expected["command"] = 'issue'
        assert vars(parser.parse_args(args)) == self.expected

    def test_parser_add_labels(self):
        parser, commands = get_test_parsers()
        edit_parser = Issue.get_parser(commands)
        args = ['edit', '-l', 'bug,todo', '1']
        self.expected['addlabels'] = 'bug,todo'
        assert vars(edit_parser.parse_args(args)) == self.expected
        args.insert(0, 'issue')
        self.expected["command"] = 'issue'
        assert vars(parser.parse_args(args)) == self.expected

    def test_parser_set_labels(self):
        parser, commands = get_test_parsers()
        edit_parser = Issue.get_parser(commands)
        args = ['edit', '-L', 'bug,todo', '1']
        self.expected['setlabels'] = 'bug,todo'
        assert vars(edit_parser.parse_args(args)) == self.expected
        args.insert(0, 'issue')
        self.expected["command"] = 'issue'
        assert vars(parser.parse_args(args)) == self.expected


class TestIssueCommandListParser(TestCase):

    def setUp(self):
        self.expected = {
            'subcommand': 'list',
            'scope': None,
            'sort': 'desc',
            'state': None,
            'labels': None,
        }

    def test_parser_list(self):
        parser, commands = get_test_parsers()
        issue_parser = Issue.get_parser(commands)
        args = ['list']
        assert vars(issue_parser.parse_args(args)) == self.expected
        args.insert(0, 'issue')
        self.expected["command"] = 'issue'
        assert vars(parser.parse_args(args)) == self.expected

    def test_parser_list_scope(self):
        parser, commands = get_test_parsers()
        issue_parser = Issue.get_parser(commands)
        args = ['list']
        assert vars(issue_parser.parse_args(args)) == self.expected
        args = ['list', '-a']
        self.expected["scope"] = 'assigned-to-me'
        assert vars(issue_parser.parse_args(args)) == self.expected
        args = ['list', '-c']
        self.expected["scope"] = 'created-by-me'
        assert vars(issue_parser.parse_args(args)) == self.expected

    def test_parser_list_scope_multi_options(self):
        parser, commands = get_test_parsers()
        issue_parser = Issue.get_parser(commands)
        args = ['list', '-a', '-c']
        self.expected["scope"] = 'created-by-me'
        assert vars(issue_parser.parse_args(args)) == self.expected
        args = ['list', '-c', '-a']
        self.expected["scope"] = 'assigned-to-me'
        assert vars(issue_parser.parse_args(args)) == self.expected

    def test_parser_list_opened_closed(self):
        parser, commands = get_test_parsers()
        issue_parser = Issue.get_parser(commands)
        args = ['list', '-A', '-C']
        self.expected["state"] = 'closed'
        assert vars(issue_parser.parse_args(args)) == self.expected
        args = ['list', '-A']
        self.expected["state"] = 'all'
        assert vars(issue_parser.parse_args(args)) == self.expected


class TestIssueCommandShowParser(TestCase):

    def test_parser_show(self):
        parser, commands = get_test_parsers()
        issue_parser = Issue.get_parser(commands)
        args = ['show', '1']
        expected = {
            'subcommand': 'show',
            'meta': False,
            'ids': ['1']
        }
        assert vars(issue_parser.parse_args(args)) == expected
        args.insert(0, 'issue')
        expected["command"] = 'issue'
        assert vars(parser.parse_args(args)) == expected


class TestIssueCommandDescribeIssue(TestCase):

    def setUp(self):
        self.patchers = dict(
            gitlab_wrapper=patch('glp.commands.GitlabWrapper'),
            edit_file=patch('glp.commands.edit_file')
        )
        self.mock = {k: v.start() for k, v in self.patchers.items()}
        self.mock["edit_file"].return_value = ["title\n", "\n", "desc\n"]
        issue = MagicMock()
        issue.title = "mocked"
        issue.description = "the description"
        issue.iid = '1'
        issue.labels = ['l1', 'l2']
        issue.assignee = {}
        issue.created_at = '2018-01-01T12:34:44.55Z'
        issue.updated_at = '2018-01-01T12:35:44.45Z'
        issue.milestone = None
        self.issue = issue

    def tearDown(self):
        for v in self.mock.values():
            v.stop()

    def test_default(self):
        with patch('builtins.print') as print_:
            Issue(Args())._describe_issue(self.issue)
        print_.assert_called_with("#1: mocked ['l1', 'l2'] (Unassigned)")

    def test_assignee(self):
        self.issue.assignee = {'username': 'tester'}
        with patch('builtins.print') as print_:
            Issue(Args())._describe_issue(self.issue, description=False)
        print_.assert_called_with("#1: mocked ['l1', 'l2'] (tester)")

    def test_desc(self):
        with patch('builtins.print') as print_:
            Issue(Args())._describe_issue(self.issue, description=True)
        assert print_.call_args_list == [
            call("#1: mocked ['l1', 'l2'] (Unassigned)"),
            call("the description")
        ]

    def test_description_as_int(self):
        with patch('builtins.print') as print_:
            Issue(Args())._describe_issue(self.issue, description=1)
        assert print_.call_args_list == [
            call("#1: mocked ['l1', 'l2'] (Unassigned)")
        ]

    def test_meta_no_milestone(self):
        with patch('builtins.print') as print_:
            Issue(Args())._describe_issue(
                self.issue,
                description=False,
                meta=True
            )
        assert any(map(
            lambda x: x[0][0].strip() == 'Milestone|test',
            print_.call_args_list
        )) is False

    def test_meta_milestone(self):
        self.issue.milestone = dict(title='test')
        with patch('builtins.print') as print_:
            Issue(Args())._describe_issue(
                self.issue,
                description=False,
                meta=True
            )
        assert any(map(
            lambda x: x[0][0].strip() == 'Milestone|test',
            print_.call_args_list
        )) == True


class TestIssueCommandCreate(TestCase):

    def setUp(self):
        self.patchers = dict(
            gitlab_wrapper=patch('glp.commands.GitlabWrapper'),
            edit_file=patch('glp.commands.edit_file')
        )
        self.mock = {k: v.start() for k, v in self.patchers.items()}
        self.wrapper_ = self.mock["gitlab_wrapper"].return_value
        self.project_ = self.wrapper_.project.return_value
        self.issue = self.project_.issues.create.return_value
        self.mock["edit_file"].return_value = ["title\n", "\n", "desc\n"]

    def tearDown(self):
        for v in self.mock.values():
            v.stop()

    def test_create(self):
        Issue(Args()).create()
        self.project_.issues.create.assert_called_once_with({
            'title': "title\n",
            'description': "\ndesc\n"
        })
        self.issue.save.assert_called_once_with()

    def test_create_raise_exception(self):
        args = Args(dict(labels='label1'))
        self.project_.issues.create.side_effect = GitlabGetError(
            response_body="{404: body}",
            response_code=404,
            error_message="message"
        )
        with self.assertRaises(GitLabPluginError) as exception:
            Issue(args).create()
        assert exception.exception.status is 1
        self.project_.issues.create.assert_called_once_with({
            'title': "title\n",
            'description': "\ndesc\n"
        })

    def test_create_edit_file_raise_error(self):
        self.mock["edit_file"].side_effect = GitLabPluginError("Issue file not found")
        with self.assertRaises(GitLabPluginError) as exception:
            Issue(Args()).create()
        assert exception.exception.status is 1


class TestIssueCommandEdit(TestCase):

    def setUp(self):
        self.patchers = dict(
            gitlab_wrapper=patch('glp.commands.GitlabWrapper'),
            editfile=patch('glp.commands.edit_file'),
        )
        self.mock = {k: v.start() for k, v in self.patchers.items()}
        self.wrapper_ = self.mock["gitlab_wrapper"].return_value
        self.project_ = self.wrapper_.project.return_value
        self.issue = MagicMock()
        self.issue.iid = 42
        self.issue.title = "title"
        self.issue.description = "description"
        self.mock["editfile"].return_value = [
            "new title\n",
            "new description\n",
            "that is longer\n"
        ]

    def tearDown(self):
        for v in self.mock.values():
            v.stop()

    def test_editor(self):
        self.project_.issues.get.return_value = self.issue
        Issue(Args(dict(id=42, editor=True))).edit()
        self.project_.issues.get.assert_called_once_with(42)
        self.mock["editfile"].assert_called_once_with(content=[
            "title\n",
            "\n",
            "description\n",
        ])
        assert self.issue.title == "new title"
        assert self.issue.description == "new description\nthat is longer\n"
        self.issue.save.assert_called_once_with()

    def test_labels_argument(self):
        args = Args(dict(id=42, addlabels="l1,l2"))
        self.issue.labels = ["lold"]
        self.project_.issues.get.return_value = self.issue
        Issue(args).edit()
        self.project_.issues.get.assert_called_once_with(42)
        assert self.issue.labels == "lold,l1,l2".split(",")
        self.issue.save.assert_called_once_with()

    def test_set_labels(self):
        args = Args(dict(id=42, setlabels="l1,l2"))
        self.issue.labels = ["lold"]
        self.project_.issues.get.return_value = self.issue
        Issue(args).edit()
        self.project_.issues.get.assert_called_once_with(42)
        assert self.issue.labels == "l1,l2".split(",")
        self.issue.save.assert_called_once_with()

    def test_add_and_set_labels(self):
        args = Args(dict(
            id=42,
            setlabels="l1,l2",
            addlabels="a1,a2"
        ))
        with self.assertRaises(GitLabPluginError) as exception:
            Issue(args).edit()
        self.issue.save.assert_not_called()

    def test_test_for_any_argument_present(self):
        with self.assertRaises(GitLabPluginError) as exception:
            Issue(Args()).edit()

    def test_set_milestone_from_id(self):
        args = Args(dict(
            id="1",
            milestone="1"
        ))
        self.project_.issues.get.return_value = self.issue
        self.project_.milestones.list.side_effect = return_list(MagicMock())
        Issue(args).edit()
        self.project_.milestones.list.assert_called_once_with(iids="1")

    def test_set_milestone_unknown_id(self):
        args = Args(dict(
            id="1",
            milestone="1"
        ))
        self.project_.issues.get.return_value = self.issue
        self.project_.milestones.list.return_value = list()
        with self.assertRaises(GitLabPluginError):
            Issue(args).edit()
        self.project_.milestones.list.assert_called_once_with(iids="1")

    def test_set_milestone_from_id_negative(self):
        args = Args(dict(
            id="1",
            milestone="-1"
        ))
        with self.assertRaises(GitLabPluginError):
            Issue(args).edit()

    def test_set_milestone_from_description(self):
        args = Args(dict(
            id="1",
            milestone="test"
        ))
        ms1 = MagicMock()
        ms2 = MagicMock()
        self.project_.milestones.list.return_value = [ms1, ms2]
        self.project_.issues.get.return_value = self.issue
        with patch('builtins.input') as input_:
            input_.return_value = "2"
            Issue(args).edit()
        self.project_.milestones.list.assert_called_once_with(search="test")
        assert self.issue.milestone_id == ms2.id

    def test_set_milestone_list_raise_exception(self):
        args = Args(dict(
            id="1",
            milestone="test"
        ))
        self.project_.milestones.list.side_effect = GitlabGetError(
            error_message="{status: 404}",
            response_code=404,
            response_body="Not found"
        )
        self.project_.issues.get.return_value = self.issue
        with self.assertRaises(GitLabPluginError):
            Issue(args).edit()

    def test_set_assignees(self):
        u1 = MagicMock()
        args = Args(dict(
            id="1",
            assignees="testu",
        ))
        self.project_.issues.get.return_value = self.issue
        users = self.wrapper_.gitlab.return_value.users
        users.list.side_effect = return_list(u1)
        Issue(args).edit()
        users.list.assert_called_once_with(search="testu")
        assert self.issue.assignee_ids == [u1.id]

    def test_set_assignees_multi_matches(self):
        u1 = MagicMock()
        u2 = MagicMock()
        args = Args(dict(
            id="1",
            assignees="testu",
        ))
        self.project_.issues.get.return_value = self.issue
        users = self.wrapper_.gitlab.return_value.users
        users.list.side_effect = return_list(u1, u2)
        with patch('builtins.input') as input_:
            input_.return_value = "2"
            Issue(args).edit()
        users.list.assert_called_once_with(search="testu")
        assert self.issue.assignee_ids == [u2.id]

    def test_set_assignees_multi_patterns(self):
        u1 = MagicMock()
        u2 = MagicMock()
        args = Args(dict(
            id="1",
            assignees="testu,user",
        ))
        self.project_.issues.get.return_value = self.issue
        users = self.wrapper_.gitlab.return_value.users
        users.list.side_effect = [[u1], [u2]]
        Issue(args).edit()
        assert self.issue.assignee_ids == [u1.id, u2.id]

    def test_set_assignees_no_user_found(self):
        u1 = MagicMock()
        args = Args(dict(
            id="1",
            assignees="testu",
        ))
        self.project_.issues.get.return_value = self.issue
        users = self.wrapper_.gitlab.return_value.users
        users.list.side_effect = return_list()
        with self.assertRaises(GitLabPluginError):
            Issue(args).edit()

    def test_unset_assignees(self):
        u1 = MagicMock()
        args = Args(dict(
            id="1",
            assignees="0",
        ))
        self.project_.issues.get.return_value = self.issue
        Issue(args).edit()
        assert self.issue.assignee_ids == ""


class TestIssueCommandShow(TestCase):

    def setUp(self):
        self.patchers = dict(
            gitlab_wrapper=patch('glp.commands.GitlabWrapper'),
        )
        self.mock = {k: v.start() for k, v in self.patchers.items()}
        self.wrapper_ = self.mock["gitlab_wrapper"].return_value
        self.project_ = self.wrapper_.project.return_value
        self.issue = MagicMock()
        self.issue.iid = 42
        self.issue.title = "title"
        self.issue.description = "description"

    def tearDown(self):
        for v in self.mock.values():
            v.stop()

    def test_show_wrong_id(self):
        self.project_.issues.get.side_effect = GitlabGetError(
            response_code=404,
            response_body="not found",
            error_message="{404: not found}"
        )
        args = Args(dict(ids=[10]))
        with self.assertRaises(GitLabPluginError) as exception:
            Issue(args).show()


class TestIssueCommand(TestCase):

    def setUp(self):
        self.patchers = dict(
            gitlab_wrapper=patch('glp.commands.GitlabWrapper'),
        )
        self.mock = {k: v.start() for k, v in self.patchers.items()}
        self.wrapper_ = self.mock["gitlab_wrapper"].return_value
        self.project_ = self.wrapper_.project.return_value

    def tearDown(self):
        for v in self.mock.values():
            v.stop()

    def test_list_no_issues(self):
        self.project_.issues.list.return_value = []
        with patch('builtins.print') as print_:
            Issue(Args()).list()
            print_.assert_called_once_with("No issues found.")

    @patch('glp.commands.Issue._describe_issue')
    def test_show(self, desc_):
        issue1 = MagicMock()
        issue2 = MagicMock()
        self.project_.issues.get.side_effect = [issue1, issue2]
        args = Args(dict(ids=['1', '2'], meta=True))
        Issue(args).show()
        assert desc_.call_args_list == [
            call(issue1, description=True, meta=True),
            call(issue2, description=True, meta=True)
        ]
        assert self.project_.issues.get.call_args_list == [
            call('1'),
            call('2')
        ]

    @patch('glp.commands.Issue._describe_issue')
    def test_run_list(self, desc_):
        issue1 = MagicMock()
        issue2 = MagicMock()
        args = Args(dict(
            subcommand='list',
            scope='all',
            sort='desc'
        ))
        self.project_.issues.list.return_value = [
            issue1,
            issue2
        ]
        issue = Issue(args)
        issue.run()
        self.mock["gitlab_wrapper"].assert_called_once_with()
        self.wrapper_.project.assert_called_once_with()
        self.project_.issues.list.assert_called_once_with(
            as_list=False,
            scope='all',
            sort='desc',
            state='opened',
            labels=None,
        )
        assert desc_.call_args_list == [
            call(issue1, description=False),
            call(issue2, description=False)
        ]

    @patch('glp.commands.Issue._describe_issue')
    def test_run_show(self, desc_):
        args = Args(dict(
            subcommand='show',
            ids=[1, 2, 3]
        ))
        self.project_.issues.get.side_effect = [
            MagicMock(),
            MagicMock(),
            MagicMock(),
        ]
        issue = Issue(args)
        issue.run()
        self.mock["gitlab_wrapper"].assert_called_once_with()
        self.wrapper_.project.assert_called_once_with()
        assert len(desc_.call_args_list) == 3

    @patch('glp.commands.edit_file')
    def test_run_create(self, edit_file_):
        edit_file_.return_value = [
            "New Issue\n",
            "description\n",
            "\n",
            "test"
        ]
        new_issue = MagicMock()
        new_issue.iid = '42'
        new_issue.title = 'New Issue'
        self.project_.issues.create.return_value = new_issue
        args = Args(dict(
            subcommand='create'
        ))
        issue = Issue(args)
        with patch('builtins.print') as print_:
            issue.run()
        self.mock["gitlab_wrapper"].assert_called_once_with()
        self.project_.issues.create.assert_called_once_with(dict(
            title=edit_file_.return_value[0],
            description="description\n\ntest"
        ))
        print_.assert_called_once_with("Created issue:\n#42: New Issue")


class TestLintCommand(TestCase):

    def setUp(self):
        import sys
        self.requests = MagicMock()
        sys.modules['requests'] = self.requests
        self.patchers = dict(
            gitlab_config=patch('glp.commands.GitLabConfig'),
            path_exists=patch('os.path.exists')
        )
        self.mock = {k: v.start() for k, v in self.patchers.items()}

    def tearDown(self):
        import sys
        del sys.modules['requests']
        for v in self.mock.values():
            v.stop()

    def test__gitlab_lint(self):
        test_data = {
            'stages': ["test"],
            'test': {
                'script': ["tox"],
                'image': 'docker:image'
            }
        }
        lint = Lint(Args())
        lint.data = test_data
        lint.config = self.mock["gitlab_config"].return_value
        lint._gitlab_lint()
        args = self.requests.post.call_args
        data = yaml.safe_load(json.loads(args[1]["data"])['content'])
        assert data == test_data

    def test_missing_file(self):
        lint = Lint(Args())
        lint.config = self.mock["gitlab_config"].return_value
        self.mock["path_exists"].return_value = False
        with self.assertRaises(GitLabPluginError) as exception:
            lint.run()


class TestMrCommand(TestCase):

    def test_get_parser(self):
        args = MagicMock()
        parser, commands = get_test_parsers()
        mr_parser = Mr.get_parser(commands)
        assert vars(mr_parser.parse_args(['create'])) == {
            'action': 'create'
        }
        args = ['mr', 'create']
        assert vars(parser.parse_args(args)) == {
            'command': 'mr',
            'action': 'create'
        }
