from argparse import ArgumentParser
from unittest import TestCase
from unittest.mock import MagicMock, patch

from gitlab.exceptions import GitlabAuthenticationError, GitlabGetError

import glp
from glp.plugin import GitLabPlugin


class TestGitLabPlugin(TestCase):

    def setUp(self):
        self.plugin = GitLabPlugin()
        self.url = 'https://gitlab.test.org'

    def test_subcommands(self):
        for cmd in [cmd for cmd in dir(self.plugin) if "_cmd" in cmd]:
            assert isinstance(getattr(self.plugin, cmd), ArgumentParser)
        assert list(self.plugin.commands.keys()) == [
            'config', 'issue', 'lint', 'mr'
        ]

    def test_config(self):
        args = self.plugin.parse(['config', '-g', '-u', 'user', self.url])
        assert args.command == "config"
        direct_args = self.plugin.parser.parse_args([
            'config',
            '-g',
            '-u',
            'user',
            self.url
        ])
        assert direct_args == args

    def test_issue(self):
        args = self.plugin.parse(['issue'])
        assert args.command == "issue"

    def test_lint(self):
        args = self.plugin.parse(['lint'])
        assert args.command == "lint"

    def test_mr(self):
        args = self.plugin.parse(['mr', 'create'])
        assert args.command == "mr"
