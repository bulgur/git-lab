import argparse
from unittest import TestCase
from unittest.mock import patch

import glp
from glp import GitLabPluginError, Opts, choose_from


class TestGetOutput(TestCase):

    @patch('subprocess.Popen')
    def test_get_output(self, popen):
        import subprocess
        process = popen.return_value
        process.communicate.return_value = (b' output data\r\n', b'')
        output = glp.get_output('command to execute')
        popen.assert_called_once_with(
            ['command', 'to', 'execute'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        process.communicate.assert_called_once_with(input=None)
        assert output == 'output data'

    @patch('subprocess.Popen')
    def test_get_output_raise_error(self, popen):
        import subprocess
        process = popen.return_value
        process.communicate.return_value = (b'', b'')
        with self.assertRaises(GitLabPluginError) as exception:
            output = glp.get_output('command to execute')
        popen.assert_called_once_with(
            ['command', 'to', 'execute'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        process.communicate.assert_called_once_with(input=None)


class TestIssueEditor(TestCase):

    def setUp(self):
        self.patchers = dict(
            popen=patch('subprocess.Popen'),
            mktemp=patch('tempfile.mktemp'),
            environ=patch('os.environ'),
            exists=patch('os.path.exists'),
        )
        self.mock = {k: v.start() for k, v in self.patchers.items()}
        self.mock["mktemp"].return_value = "tmp/file"
        self.mock["exists"].return_value = True

    def tearDown(self):
        for v in self.mock.values():
            v.stop()

    def test_edit_file(self):
        with patch('builtins.open') as open_mock:
            tmp_file = open_mock.return_value.__enter__.return_value
            tmp_file.readlines.return_value = ["this", "is", "a", "test"]
            ret = glp.edit_file()
            tmp_file.writelines.assert_not_called()
        assert ret == ["this", "is", "a", "test"]
        self.mock["environ"].get.assert_called_once_with("EDITOR", "nano")

    def test_edit_file_with_initial_content(self):
        with patch('builtins.open') as open_mock:
            tmp_file = open_mock.return_value.__enter__.return_value
            tmp_file.readlines.return_value = ["this", "is", "a", "test"]
            ret = glp.edit_file(content=["initial", "content"])
            tmp_file.writelines.assert_called_once_with(["initial", "content"])
        assert ret == ["this", "is", "a", "test"]
        self.mock["environ"].get.assert_called_once_with("EDITOR", "nano")

    def test_edit_file_raise_error_file_not_found(self):
        self.mock["exists"].return_value = False
        with self.assertRaises(GitLabPluginError):
            glp.edit_file()


class TestOpts(TestCase):

    def setUp(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-1', dest='attr1', action='store')
        parser.add_argument('-2', dest='attr2', action='store')
        parser.add_argument('-3', dest='attr3', action='store')
        self.args = parser.parse_args('-1 1 -2 aqq -3 3'.split())

    def test_init(self):
        opts = Opts(self.args)
        assert vars(opts) == vars(self.args)
        assert opts.attr1 == self.args.attr1

    def test_get(self):
        opts = Opts(self.args)
        assert opts.attr1 == "1"
        assert opts.attr2 == "aqq"
        assert opts.attr3 == "3"
        assert opts.attr4 is None

    def test_pop(self):
        opts = Opts(self.args)
        attr1 = opts.pop("attr1")
        assert attr1 == "1"
        assert opts.attr1 is None
        assert opts.attr2 == "aqq"

    def test_pop_unknown_attr(self):
        opts = Opts(self.args)
        attr = opts.pop("unknown_attr")
        assert attr is None


class TestChooseFrom(TestCase):

    def setUp(self):
        self.opts = {
            "arg1": 1,
            "arg2": 2,
            "arg3": 3
        }

    def test_choose_from(self):
        with patch('builtins.input') as input_:
            input_.return_value = "2"
            resp = choose_from(self.opts)
        assert resp == 2

    def test_bad_answer(self):
        with patch('builtins.input') as input_:
            input_.return_value = "asdf"
            with self.assertRaises(GitLabPluginError):
                choose_from(self.opts)

    def test_empty_answer(self):
        with patch('builtins.input') as input_:
            input_.return_value = ""
            resp = choose_from(self.opts)
        assert resp == 1

    def test_not_in_range(self):
        for answer in ["-1", "-2", "4", "10"]:
            with patch('builtins.input') as input_:
                input_.return_value = answer
                with self.assertRaises(GitLabPluginError):
                    choose_from(self.opts)

    def test_empty_list(self):
        with patch('builtins.input') as input_:
            input_.return_value = ""
            with self.assertRaises(GitLabPluginError):
                choose_from(dict())
