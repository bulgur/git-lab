from unittest import TestCase
from unittest.mock import patch

from glp import GitLabPluginError
from glp.config import GitLabConfig, ReadOnlyError


class TestConfig(TestCase):

    def setUp(self):
        self.patchers = dict(
            git_config=patch('git.config.GitConfigParser'),
            git_repo=patch('git.Repo'),
        )
        self.mock = {k: v.start() for k, v in self.patchers.items()}

    def tearDown(self):
        for v in self.mock.values():
            v.stop()

    def test_local_get_without_section(self):
        for read_only in (True, False):
            config = GitLabConfig(read_only=read_only)
            mocked = self.mock['git_repo'].return_value
            if read_only:
                mocked = mocked.config_reader.return_value
            else:
                mocked = mocked.config_writer.return_value
            mocked.has_section.return_value = False
            mocked.has_section.assert_not_called()
            assert config.not_existing_attr is None
            mocked.has_section.assert_called_once_with('git-lab')

    def test_global_get_without_section(self):
        config = GitLabConfig(local=False)
        mocked = self.mock['git_config'].return_value
        mocked.has_section.return_value = False
        mocked.has_section.assert_not_called()
        assert config.not_existing_attr is None
        mocked.has_section.assert_called_once_with('git-lab')

    def test_get_own_attrs(self):
        config = GitLabConfig(local=False)
        assert config.read_only
        assert config._section == 'git-lab'

    def test_get_from_git_config(self):
        config = GitLabConfig(local=False)
        mocked = self.mock['git_config'].return_value
        mocked.has_section.return_value = True
        mocked.get.return_value = 'from config'
        assert config.value_from_config == 'from config'
        mocked.get.assert_called_once_with('git-lab', 'valuefromconfig')

    def test_set_own_attrs(self):
        config = GitLabConfig(local=False)
        assert config.read_only
        ret = config.read_only = False
        assert ret is False
        assert config.read_only is False
        config.read_only = True
        with self.assertRaises(ReadOnlyError):
            ret = config.not_exising_attr = 123

    def test_set_config_attrs(self):
        config = GitLabConfig(local=False, read_only=False)
        mocked = self.mock['git_config'].return_value
        mocked.has_section.side_effect = (False, True)
        assert config.read_only is False
        ret = config.new_setting = 'aqq'
        assert ret == 'aqq'
        mocked.has_section.assert_called_once_with('git-lab')
        mocked.add_section.assert_called_once_with('git-lab')
        mocked.set.assert_called_once_with('git-lab', 'newsetting', 'aqq')
        mocked.has_section.reset_mock()
        mocked.add_section.reset_mock()
        mocked.set.reset_mock()
        ret = config.other_setting = 'AQQ'
        assert ret == 'AQQ'
        mocked.has_section.assert_called_once_with('git-lab')
        mocked.add_section.assert_not_called()
        mocked.set.assert_called_once_with('git-lab', 'othersetting', 'AQQ')

    def test_gitlab_host(self):
        config = GitLabConfig(local=True, read_only=True)
        assert "aa" == config.gitlab_host
