import os
from collections import namedtuple

import pytest
from pytest import fixture

from glp.plugin import GitLabPlugin


@fixture
def run_in_tmpdir(tmpdir):
    current = os.getcwd()
    os.chdir(tmpdir)
    yield os.getcwd()
    os.chdir(current)
    import shutil
    try:
        shutil.rmtree(tmpdir)
    except Exception:
        pass


@fixture
def repo(run_in_tmpdir):
    import git
    git.Repo.init()
    yield git.Repo()


CmdArg = namedtuple("CmdArg", ["name", "arg", "value"])
config_command_params = [
    (
        CmdArg("user", "-u", "test_u"),
        CmdArg("gitlab_url", None, "https://gitlab.example.com")
    ),
    (
        CmdArg("user", "-u", "test_u"),
        CmdArg("personal_token_cmd", "--personal-token-cmd", "a token cmd"),
        CmdArg("gitlab_url", None, "https://gitlab.example.com")
    ),
    (
        CmdArg("user", "-u", "test_u"),
        CmdArg("project_path", "--project-path", "group_name/project_name"),
        CmdArg("personal_token_cmd", "--personal-token-cmd", "a token cmd"),
        CmdArg("gitlab_url", None, "https://gitlab.example.com")
    )
]


@pytest.mark.parametrize("params", config_command_params)
def test_config_command(repo, params):
    plugin = GitLabPlugin()
    args = sum([(cmdarg.arg, cmdarg.value) for cmdarg in params], ('config', ))
    args = plugin.parse(list(filter(None, args)))
    cmd = plugin.commands[args.command](args)
    cmd.run()
    config = repo.config_reader()
    assert config.has_section('git-lab')
    for cmdarg in params:
        assert config.get('git-lab', cmdarg.name.replace("_", "")) == cmdarg.value
