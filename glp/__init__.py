class GitLabPluginError(Exception):

    def __init__(self, description, status=1):
        super(GitLabPluginError, self).__init__(self, description)
        self.status = status


class Opts(object):
    """
    Wrapper class around :py:class:`argparse.Namespace` object. Adds one method
    to remove attribute from this object
    """

    def __init__(self, namespace):
        for key, value in vars(namespace).items():
            setattr(self, key, value)

    def __getattr__(self, what):
        return None

    def pop(self, what):
        """
        Remove requested attribute from this instance and return it's value

        Args:
            what (string): Requested attribute

        Returns:
            a value or None
        """
        ret = getattr(self, what)
        setattr(self, what, None)
        return ret


def edit_file(content=None):
    """
    Edit a temporary file using external editor and return it's content.
    If optional argument `content` is provided it will be written to the
    file before eiditing it.

    Args:
        content(list): Optional list of lines to write to the temporary file
                       before editing it
    """
    import os
    import subprocess
    import tempfile
    tmp_file = tempfile.mktemp()
    if content is not None:
        with open(tmp_file, 'w') as fd:
            fd.writelines(content)
    editor = os.environ.get('EDITOR', 'nano')
    process = subprocess.Popen([editor, tmp_file])
    process.wait()
    if not os.path.exists(tmp_file):
        raise GitLabPluginError("Unable to find issue file. Have you saved it?")
    with open(tmp_file, 'r') as fd:
        content = fd.readlines()
    return content


def get_output(cmd):
    """
    Run given command and return output from commands standard output.

    Args:
        cmd (string): Command to run

    Returns:
        string: Output of a command

    Raises:
        GitLabPluginError: In case when there was no output

    .. _get_output:

    """
    from subprocess import Popen, PIPE
    process = Popen(cmd.split(), stdout=PIPE, stderr=PIPE)
    output, _ = process.communicate(input=None)
    if not output:
        raise GitLabPluginError(f"No token from a command: {cmd}")
    return output.decode('utf-8').strip()


def choose_from(options):
    """
    Print a list of options and user to choose one.

    Args:
        options (dict): Dict of options to choose from, where key is a
                        description of the item.

    Raises:
        GitLabPluginError: In case of lack of options or a problem with entered
                           value by the user

    Returns:
        selected option
    """
    if not options or not len(options.keys()):
        raise GitLabPluginError("No options provided.")
    keys = list(options.keys())
    print("Your request was ambiguous. Please choose right one:")
    print("\n".join(map(lambda x: f"{x[0]}) {x[1]}",
                        enumerate(keys, 1))))
    answer = input("\n").strip()
    if len(answer) == 0:
        return options[keys[0]]
    try:
        answer = int(answer) - 1
    except ValueError as exception:
        raise GitLabPluginError(f"Invalid answer: {answer}")
    if 0 <= answer < len(keys):
        return options[keys[answer]]
    else:
        raise GitLabPluginError(f"No such option: {answer}")


def raise_from_gitlab_exception(exception, message=""):
    """
    Raises GitLabPluginError exception filled using error codes and message
    from exceptions based on GitlabError from gitlab module.

    Args:
        exception: a GitlabError exception object
        message (string): Additional message to be included at the begining

    Raises:
        GitLabPluginError: always
    """
    raise GitLabPluginError(
        f"{message}"
        f"\nStatus: {exception.response_code}"
        f"\nResponse: {exception.response_body}"
        f"\nMessage: {exception.error_message}",
        status=1
    )
