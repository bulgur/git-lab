from datetime import datetime

import dateutil.parser
from gitlab.exceptions import GitlabError

from glp import (
    GitLabPluginError,
    raise_from_gitlab_exception,
    choose_from,
    edit_file
)
from glp.config import GitLabConfig
from glp.wrapper import GitlabWrapper


class ConfigMixin(object):
    """
    A mixin class that adds :class:`~glp.config.GitLabConfig` instance in
    read-only mode.
    """

    def __init__(self):
        self.config = GitLabConfig()


class GitlabMixin(object):
    """
    A mixin class that adds :class:`~glp.wrapper.GitlabWrapper` instance and
    project instance.
    """

    def __init__(self):
        self.wrapper = GitlabWrapper()
        self.project = self.wrapper.project()


class CommandBase(object):
    """
    A base class for all commands. Should not be used alone, but inherited.

    Args:
        args (argparse.Namespace): A Namespace object from
            `argparse.ArgumentParser.parse_args`
    """

    def __init__(self, args):
        super(CommandBase, self).__init__()
        self.args = args

    @classmethod
    def command_name(cls):
        return cls.__name__.lower()

    @staticmethod
    def get_parser(parser):
        """
        Creates a subparser and defines options and arguments for it.

        Args:
            parser (argparse.ArgumentParser):  a top level parser

        Raises:
            NotImplementedError: This is just a placeholder method, which
                should be overwritten
        """
        raise NotImplementedError

    def run(self):
        """
        Perform action of this command.

        Raises:
            NotImplementedError: This is just a placeholder method, which
                should be overwritten
        """
        raise NotImplementedError


class Config(CommandBase):
    """
    A :class:`~CommandBase` derivative responsible for `config` command.
    Command responsible for altering config for this plugin.
    """

    @staticmethod
    def get_parser(parser):
        """
        Creates a subparser and defines options and arguments for it.

        Args:
            parser (argparse.ArgumentParser): A subparser obtained from calling
                :py:meth:`argparse.ArgumentParser.add_subparsers`

        Returns:
            argparse.ArgumentParser: a subparser for config command
        """
        config_parser = parser.add_parser(
            'config',
            help="Alter gits configuration for Gitlab"
        )
        config_parser.add_argument(
            '--personal-token-cmd',
            action='store',
            help="""
            A command to be used to get Gitlab's personal api token.
            This command must output only token on it's standard output.
            """
        )
        config_parser.add_argument(
            '--project-path',
            action='store',
            help="""
            Path to access project using url (without url hostname part).
            Usually this is <group name>/<project name>. If not set,
            GitLabPlugin will try to guess it from url address of a remote
            `origin` if present.
            """
        )
        config_parser.add_argument(
            '-g',
            '--global',
            action='store_false',
            dest='local',
            help="""
            All setting will be written to global git config instead of local
            one for this repo.
            """
        )
        config_parser.add_argument(
            '-u',
            '--user',
            action='store',
            required=True,
            help="""Gitlab user to be used."""
        )
        config_parser.add_argument(
            'gitlab',
            action='store',
            help="""
            A http/https url for Gitlab instance to be used with this repo.
            """
        )
        return config_parser

    def run(self):
        config = GitLabConfig(local=self.args.local, read_only=False)
        if self.args.personal_token_cmd is not None:
            config.personal_token_cmd = self.args.personal_token_cmd
        if self.args.project_path is not None:
            config.project_path = self.args.project_path
        if not self.args.gitlab:
            raise GitLabPluginError("Missing mandatory `gitlab` argument")
        if not self.args.user:
            raise GitLabPluginError("Missing mandatory `user` argument")
        config.gitlab_url = self.args.gitlab
        config.user = self.args.user


class Issue(CommandBase, GitlabMixin):
    """
    A :class:`~CommandBase` derivative responsible for `issue` command.
    It manages issues in related Gitlab instance.
    """

    @staticmethod
    def get_parser(parser):
        """
        Creates a subparser and defines options and arguments for it.

        Args:
            parser (argparse.ArgumentParser): A subparser obtained from calling
                :py:meth:`argparse.ArgumentParser.add_subparsers`

        Returns:
            argparse.ArgumentParser: a subparser for config command
        """
        issue_parser = parser.add_parser(
            'issue',
            help="Manage issues"
        )
        sub_cmds = issue_parser.add_subparsers(
            dest='subcommand',
            help='A subcommand to perform a task'
        )
        create_issue = sub_cmds.add_parser(
            'create',
            help='Create issue'
        )
        create_issue.add_argument(
            '-a',
            '--assignees',
            action='store',
            help='Coma-separated list of IDs or names of users to be assigned'
        )
        create_issue.add_argument(
            '-l',
            '--labels',
            action='store',
            help='Add labels to the issue, a comma-separated list'
        )
        create_issue.add_argument(
            '-m',
            '--milestone',
            action='store',
            help="Set milestone. Provide it's id or part of name"
        )
        edit_issue = sub_cmds.add_parser(
            'edit',
            help='Modify the issue'
        )
        edit_issue.add_argument(
            '-a',
            '--assignees',
            action='store',
            help='Coma-separated list of IDs or names of users to be assigned'
        )
        edit_issue.add_argument(
            '-e',
            '--editor',
            action='store_true',
            help='Edit issue title/description using external editor'
        )
        edit_issue.add_argument(
            '-l',
            '--labels-add',
            action='store',
            dest='addlabels',
            help='Add additional labels to existing ones'
        )
        edit_issue.add_argument(
            '-L',
            '--labels-set',
            action='store',
            dest='setlabels',
            help='Set issue labels (will replace old ones)'
        )
        edit_issue.add_argument(
            '-m',
            '--milestone',
            action='store',
            help="Set milestone. Provide it's id or part of name"
        )
        edit_issue.add_argument(
            'id',
            help='Issue ID'
        )
        list_issues = sub_cmds.add_parser(
            'list',
            help="List issues"
        )
        list_issues.add_argument(
            '-a',
            '--assigned',
            action='store_const',
            const='assigned-to-me',
            dest='scope',
            help='List issues assigned to me'
        )
        list_issues.add_argument(
            '-c',
            '--created',
            action='store_const',
            const='created-by-me',
            dest='scope',
            help='List issues created by me'
        )
        list_issues.add_argument(
            '-r',
            action='store_const',
            const='asc',
            dest='sort',
            default='desc',
            help='Sort issues in reverse order'
        )
        list_issues.add_argument(
            '-C',
            '--closed',
            action='store_const',
            const='closed',
            dest='state',
            help='List only closed issues (opened is the default)'
        )
        list_issues.add_argument(
            '-A',
            '--all',
            action='store_const',
            const='all',
            dest='state',
            help='List opend and closed issues'
        )
        list_issues.add_argument(
            '-l',
            '--labels',
            dest='labels',
            help='Comma-separated list of labels to filter issues'
        )
        list_issues.add_argument(
            '-n',
            '--no-label',
            action='store_const',
            const='No Label',
            dest='labels',
            help='Show issues without any label'
        )
        show_issue = sub_cmds.add_parser(
            'show',
            help="Show issue(s)"
        )
        show_issue.add_argument(
            '-m',
            '--meta',
            action='store_true',
            help='Show metadata information about issue'
        )
        show_issue.add_argument(
            'ids',
            nargs='+',
            help='Issue id(s)'
        )
        return issue_parser

    def run(self):
        subcommand = self.args.pop("subcommand")
        getattr(self, subcommand)()

    def create(self):
        lines = edit_file()
        try:
            issue = self.project.issues.create(dict(
                title=lines[0],
                description="".join(lines[1:])
            ))
        except GitlabError as exception:
            raise_from_gitlab_exception(exception, "Unable to create issue")
        if self.args.labels:
            issue.labels = self.args.labels.split(",")
        self._handle_common_args(issue)
        issue.save()
        print(f"Created issue:\n#{issue.iid}: {issue.title}")

    def edit(self):
        issue_id = self.args.pop("id")
        if not any(vars(self.args).values()):
            raise GitLabPluginError("At least one option is needed. See help.")
        issue = self.project.issues.get(issue_id)
        if self.args.editor is True:
            content = [
                f"{issue.title}\n",
                "\n",
                f"{issue.description}\n",
            ]
            content = edit_file(content=content)
            issue.title = content[0].strip()
            issue.description = "".join(content[1:])
        if self.args.addlabels is not None and self.args.setlabels is not None:
            raise GitLabPluginError(
                "Ambiguous use of --labels-add and --labels-set. "
                "Only one at a time is allowed."
            )
        if self.args.addlabels:
            issue.labels += self.args.addlabels.split(",")
        elif self.args.setlabels:
            issue.labels = self.args.setlabels.split(",")
        self._handle_common_args(issue)
        issue.save()

    def list(self):
        got_issues = False
        for issue in self.project.issues.list(as_list=False,
                                              scope=self.args.scope,
                                              sort=self.args.sort,
                                              state=(self.args.state or
                                                     'opened'),
                                              labels=self.args.labels):
            got_issues = True
            self._describe_issue(issue, description=False)
        else:
            if got_issues is not True:
                print("No issues found.")

    def show(self):
        for iid in self.args.ids:
            try:
                issue = self.project.issues.get(iid)
            except GitlabError as exception:
                raise_from_gitlab_exception(
                    exception,
                    f"Unable to get issue {iid}:"
                )
            self._describe_issue(issue, description=True, meta=self.args.meta)

    def _format_time(self, isotime):
        return datetime.strftime(
            dateutil.parser.parse(isotime),
            "%H:%M:%S %d.%m.%Y %z"
        )

    def _describe_issue(self,
                        issue,
                        description=False,
                        meta=False,
                        comments=False):
        print(
            f"#{issue.iid}: {issue.title} {issue.labels} "
            f"({(issue.assignee or {}).get('username', 'Unassigned')})"
        )
        if description is True:
            print(f"{issue.description}")
        if meta is True:
            created_at = self._format_time(issue.created_at)
            updated_at = self._format_time(issue.updated_at)
            print("\n" + "".join(["="] * 42))
            data = dict(
                status=issue.state,
                created=created_at,
                updated=updated_at,
                upvotes=issue.upvotes,
                downvotes=issue.downvotes
            )
            if issue.milestone is not None:
                data['milestone'] = issue.milestone['title']
            for key, value in data.items():
                print(f"{(key.capitalize())!s:>12}|{value!s:<29}")

    def _get_assignees(self, assignees):
        ret = list()
        if assignees == "0":
            return ""
        for pattern in assignees.split(","):
            users = self.wrapper.gitlab().users.list(search=pattern)
            if len(users) == 0:
                raise GitLabPluginError(
                    f"Couldn't find user matching: {pattern}"
                )
            elif len(users) == 1:
                ret.append(users[0].id)
            else:
                options = {f"{u.name}": u.id for u in users}
                ret.append(choose_from(options))
        return ret

    def _get_milestone(self, milestone):
        try:
            if milestone.isdigit():
                ret = self.project.milestones.list(iids=milestone)
                if len(ret) == 1:
                    return ret[0]
                raise GitLabPluginError(
                    f"Unable to get milestone with id {milestone}."
                )
            else:
                milestones = dict()
                for m in self.project.milestones.list(search=milestone):
                    key = f"#{m.iid}: {m.title}"
                    milestones[key] = m
                return choose_from(milestones)
        except GitlabError as exception:
            raise_from_gitlab_exception(
                exception,
                f"Unable to get milestone matching: {milestone}"
            )

    def _handle_common_args(self, issue):
        if self.args.milestone:
            if self.args.milestone == "0":
                issue.milestone_id = self.args.milestone
            else:
                milestone = self._get_milestone(self.args.milestone)
                issue.milestone_id = milestone.id
        if self.args.assignees:
            issue.assignee_ids = self._get_assignees(self.args.assignees)


class Lint(CommandBase, ConfigMixin):
    """
    A :class:`~CommandBase` derivative responsible for `lint` command.
    Makes it possible to validate `.gitlab-ci.yml` file against Gitlab
    linter before commiting it to the repo ans pushing upstream.
    """

    @staticmethod
    def get_parser(parser):
        """
        Creates a subparser and defines options and arguments for it.

        Args:
            parser (argparse.ArgumentParser): A subparser obtained from calling
                :py:meth:`argparse.ArgumentParser.add_subparsers`

        Returns:
            argparse.ArgumentParser: a subparser for lint command
        """
        lint_parser = parser.add_parser(
            'lint',
            help="Manage Merge Requests"
        )
        return lint_parser

    def run(self):
        import os
        import yaml
        self.config = GitLabConfig()
        ci_config = '.gitlab-ci.yml'
        if os.path.exists(ci_config):
            with open(ci_config, 'r') as f:
                self.data = yaml.safe_load(f)
            self._gitlab_lint()
        else:
            raise GitLabPluginError(f"{ci_config} file not found.")

    def _gitlab_lint(self):
        import requests
        import json
        url = f'{self.config.gitlaburl}/api/v4/ci/lint'
        data = json.dumps(self.data)
        data = '{"content": "' + data.replace('"', '\\"') + '"}'
        print(f"Validating against {url}")
        response = requests.post(
            url,
            headers={
                "Content-Type": "application/json",
            },
            data=data
        )
        resp = response.json()
        if 'status' in resp:
            print(f"Status: {resp['status']}")
            if resp['status'] != 'valid':
                for error in resp['errors']:
                    print(f" - {error}")
        else:
            print(f"Request failed: {resp['error']}")


class Mr(CommandBase, GitlabMixin):
    """
    A :class:`~CommandBase` derivative responsible for `mr` command.
    It manages Merge Requests in related Gitlab instance.
    """

    @staticmethod
    def get_parser(parser):
        """
        Creates a subparser and defines options and arguments for it.

        Args:
            parser (argparse.ArgumentParser): A subparser obtained from calling
                :py:meth:`argparse.ArgumentParser.add_subparsers`

        Returns:
            argparse.ArgumentParser: a subparser for config command
        """
        mr_parser = parser.add_parser(
            'mr',
            help="Manage Merge Requests"
        )
        mr_parser.add_argument(
            'action',
            action='store',
            choices=[
                'create'
            ],
            help="""
            Action to perform
            """
        )
        return mr_parser

    def run(self):
        pass
