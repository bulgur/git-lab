import os

import git


class ReadOnlyError(Exception):
    """
    Exception raised in case of an attempt to alter git configuration while
    in read only mode

    .. _config.ReadOnlyError:

    """
    pass


class GitLabConfig(object):
    """
    A wrapper class around **git.config.GitConfigParser** class.
    It wraps accessing proper config depending on the status of `local`
    attribute. Automatically creates a proper section in config when setting some
    values if `read_only` is False.

    Args:
        local (bool): if set to True, which is default, will use config from
            local repository, otherwise it will use **$HOME/.gitconfig**

        read_only (bool): if set to True, which is default, will prevent from
                      altering underlying config. It will raise
                      :class:`~config.ReadOnlyError` in case of an attempt to
                      set any attribute not belonging to this class directly.

    Raises:
        config.ReadOnlyError: In case of attempt to alter config file in
            read only mode

    .. _config.GitLabConfigConfig:

    """

    def __init__(self, local=True, read_only=True):
        if local:
            if read_only is False:
                self._config = git.Repo().config_writer()
            else:
                self._config = git.Repo().config_reader()
        else:
            self._config = git.config.GitConfigParser(
                os.path.join(os.environ['HOME'], '.gitconfig'),
                read_only=read_only
            )
        self._section = 'git-lab'
        self.read_only = read_only

    @property
    def gitlab_host(self):
        return "aa"

    def __getattr__(self, attr):
        if self._config.has_section(self._section):
            return self._config.get(self._section, attr.replace("_", ""))
        else:
            return None

    def __setattr__(self, attr, value):
        if attr in ['read_only', '_config', '_section']:
            self.__dict__[attr] = value
        elif not self.read_only:
            if not self._config.has_section(self._section):
                self._config.add_section(self._section)

            self._config.set(self._section, attr.replace("_", ""), value)
        else:
            raise ReadOnlyError

        return value
