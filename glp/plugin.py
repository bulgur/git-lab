import argparse

from glp import commands


class GitLabPlugin(object):
    """
    A main class for this package. Connects everything together.
    It's responsible for parsing command line and running proper commands.
    """

    def __init__(self):
        self.__parser = argparse.ArgumentParser(
            prog="git-lab",
            description="Manage Gitlab project using git itself"
        )
        self.__commands = self.__parser.add_subparsers(
            dest='command',
            help="Subcommands"
        )
        self.commands = {
            c.command_name(): c
            for c in commands.CommandBase.__subclasses__()
        }
        for c in self.commands.values():
            c.get_parser(self.__commands)

    def parse(self, args):
        """
        Parse argument list with a argument parser.

        Args:
            args (list): A list of arguments as strings to be parsed

        Returns:
            argparse.Namespace: A Namespace with parsed arguments

        Raises:
            SystemExit: Under-the-hood ArgumentParser raises this
                 exception in case of an error

        """
        return self.__parser.parse_args(args)

    @property
    def parser(self):
        """
        Access under-the-hood parser.
        """
        return self.__parser

