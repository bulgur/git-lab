import gitlab

from glp import (
    get_output,
    raise_from_gitlab_exception,
    GitLabPluginError
)
from glp.config import GitLabConfig


class GitlabWrapper(object):
    """
    A wrapper class aroud gitlab.Gitlab. It simplifies it's usage by exposing
    methods that already perform actions needed to focus on current project
    """

    def __init__(self):
        self.config = GitLabConfig()
        self._gitlab = None

    def auth(self):
        """
        Creates gitlab.Gitlab instance and performs authentication based on
        provided configuration. Currently it supports authenticating with
        private token only.

        Raises:
            GitLabPluginError: in case of any failure with connecting to Gitlab
                          instance or performing authentication process.
        """
        if self.config.personal_token_cmd is not None:
            self._gitlab_from_token()
        else:
            self._gitlab_annonymously()
        if self._gitlab is None:
            raise GitLabPluginError(
                f"Unable to create connection instance to Gitlab: "
                f"{self.config.gitlab_url}"
            )
        try:
            self._gitlab.auth()
        except gitlab.exceptions.GitlabAuthenticationError as exception:
            raise_from_gitlab_exception(
                exception,
                f"Unable to connect to Gitlab: {self.config.gitlab_url}"
            )
        except gitlab.exceptions.GitlabGetError as exception:
            raise_from_gitlab_exception(
                exception,
                "Got error response from Gitlab server: "
                f"{self.config.gitlab_url}"
            )

    def gitlab(self):
        """
        Return authenticated instance of Gitlab.

        Returns:
            gitlab instance
        """
        if not self._gitlab:
            self.auth()
        return self._gitlab

    def project(self):
        """
        Retrieves a gitlab.GitlabProject matching this Git repository.
        May perform authentication process using
        :func:`~glp.wrapper.GitlabWrapper.auth()`

        Returns:
            Instance of gitlab.GitlabProject

        Raises:
            GitLabPluginError: if there were any failures with obtaining Gitlab
                          project
        """
        try:
            return self.gitlab().projects.get(self.config.project_path)
        except gitlab.exceptions.GitlabError as exception:
            raise_from_gitlab_exception(
                exception,
                "Unable to retriev project: "
                f"{self.config.project_path}"
            )

    def _gitlab_from_token(self):
        try:
            token = get_output(self.config.personal_token_cmd)
        except GitLabPluginError as exception:
            return
        self._gitlab = gitlab.Gitlab(
            url=self.config.gitlab_url,
            private_token=token
        )

    def _gitlab_annonymously(self):
        pass
